"""REST client handling, including PropXStream base class."""

from __future__ import annotations

from pathlib import Path
from typing import Any, Callable, Iterable

import requests
import datetime
from pendulum import parse
from singer_sdk.authenticators import APIKeyAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.pagination import BaseAPIPaginator  # noqa: TCH002
from singer_sdk.streams import RESTStream
from dateutil import parser
from http import HTTPStatus
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError


_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]



class PropXStream(RESTStream):
    """PropX stream class."""
    fetching_for_parent = False
    fetching_active_jobs = False
    fetching_not_started_jobs = False
    extra_retry_statuses = [429, 104, 401,101] #Propx seems to give 401 on 429
    @property
    def url_base(self) -> str:
        return "https://publicapis.propx.com/api/v1/"

    records_jsonpath = "$[*]"

    next_page_token_jsonpath = "$.next_page"  # noqa: S105

    @property
    def http_headers(self) -> dict:

        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        # If not using an authenticator, you may also provide inline auth headers:
        headers["authorization"] = self.config.get("api_key")  # noqa: ERA001
        return headers

    def get_next_page_token(self, response, previous_token=0):
        
        res_json = response.json().get("pagination")
        actual_page = int(res_json.get("page_no"))
        total_records = int(res_json.get("total"))
        per_page = int(res_json.get("per_page"))
        actual_records = per_page * actual_page
        data = response.json()
        if "data" in  data:
            if len(data['data']) == 0:
                return None 
        if actual_records < total_records:
            return actual_page + 1
        #Done fetching active jobs. Now we need to trigger fetching not started jobs.
        if self.fetching_active_jobs and self.config.get("sync_not_started_jobs",False) and self.name=="jobs":
            return "start_not_started"
        elif self.fetching_not_started_jobs:
            #Since we are in this block means not-fetching has reached end of pagination as well
            self.fetching_not_started_jobs = False
            return None
        else:    
            return None

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context) + datetime.timedelta(seconds=1)
        return rep_key or start_date

    def get_url_params(
        self,
        context: dict | None,  # noqa: ARG002
        next_page_token: Any | None,  # noqa: ANN401
    ) -> dict[str, Any]:
        params: dict = {}
        start_date = self.get_starting_timestamp(context) or datetime.datetime(2000, 1, 1)
        #Verify state is loaded
        jobs_state_loaded = self.name == "jobs" and start_date.year > 2000
         
        only_sync_active_jobs = self.config.get("only_sync_active_jobs",False)
        sync_not_started_jobs = self.config.get("sync_not_started_jobs",False)

        if jobs_state_loaded and only_sync_active_jobs and not sync_not_started_jobs:
            params["working_status"] = "Active"
            self.fetching_active_jobs = True
        elif only_sync_active_jobs and sync_not_started_jobs:
            #Fetch both active and not started jobs
            params["working_status"] = "Active"
            self.fetching_active_jobs = True
        elif sync_not_started_jobs:
            #sync not started jobs only
            next_page_token = "start_not_started"


        #If active jobs have been fetched start fetching not started jobs
        if next_page_token is not None:
            if next_page_token=="start_not_started":
                params["working_status"] = "Not Started"  
                self.fetching_active_jobs = False
                self.fetching_not_started_jobs = True
                next_page_token = 1


        if next_page_token:
            params["page_no"] = next_page_token
        if self.name=="job_silos":
            params["current_job"] = context.get("job_name")    
        return params

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        yield from extract_jsonpath(self.records_jsonpath, input=response.json())

    def log_retry(self,response):
        self.logger.warn(f"Request for {response.url} failed with {response.status_code} with response {response.text}")    

    def validate_response(self, response: requests.Response) -> None:
       
        if (
            response.status_code in self.extra_retry_statuses
            or HTTPStatus.INTERNAL_SERVER_ERROR
            <= response.status_code
            <= max(HTTPStatus)
        ):
            msg = self.response_error_message(response)
            self.log_retry(response)
            raise RetriableAPIError(msg, response)

        if (
            HTTPStatus.BAD_REQUEST
            <= response.status_code
            < HTTPStatus.INTERNAL_SERVER_ERROR
        ):
            msg = self.response_error_message(response)
            self.log_retry(response)
            raise FatalAPIError(msg)    
