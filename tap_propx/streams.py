"""Stream type classes for tap-propx."""

from __future__ import annotations

import pendulum
import typing as t
from datetime import datetime
from pathlib import Path
from typing import Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_propx.client import PropXStream


class JobsStream(PropXStream):
    name = "jobs"
    path = "jobs"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = "last_modified_date"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("name", th.StringType),
        th.Property("order_no", th.StringType),
        th.Property("start_date", th.DateTimeType),
        th.Property("end_date", th.DateTimeType),
        th.Property("special_notes", th.StringType),
        th.Property("accepted_notes", th.StringType),
        th.Property("loader_notes", th.StringType),
        th.Property("intransit_notes", th.StringType),
        th.Property("atdestination_note", th.StringType),
        th.Property("added_date", th.StringType),
        th.Property("last_modified_date", th.DateTimeType),
        th.Property("status", th.NumberType),
        th.Property("use_appt_time", th.NumberType),
        th.Property("prefil_date", th.StringType),
        th.Property("spotting_date", th.StringType),
        th.Property("carrier_po", th.StringType),
        th.Property("is_geofence", th.NumberType),
        th.Property("job_type", th.StringType),
        th.Property("working_status", th.StringType),
        th.Property("added_by", th.StringType),
        th.Property("last_modified_by", th.StringType),
        th.Property("destination_latitude", th.StringType),
        th.Property("destination_longitude", th.StringType),
        th.Property("destination_name", th.StringType),
        th.Property("destination_address", th.StringType),
        th.Property("po_owner", th.StringType),
        th.Property("customer_name", th.StringType),
        th.Property("customer_markup", th.NumberType),
        th.Property("logistict_company_name", th.StringType),
        th.Property("service_company_name", th.StringType),
        th.Property("region_name", th.StringType),
        th.Property("district_name", th.StringType),
        th.Property("crew_name", th.StringType),
        th.Property("wells", th.NumberType),
    ).to_dict()

    def get_child_context(self, record: dict, context: dict | None) -> dict | None:
        return {
            "job_id": record["id"],
            "working_status":record["working_status"],
            "job_name":record["name"]
        }

class LoadsStream(PropXStream):
    name = "loads"
    path = "loads"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = "load_last_updated_date"

    parent_stream_type = JobsStream
    ignore_parent_replication_keys = False
    loads_dict = {}
    path = "jobs/{job_id}/loads"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("po_id", th.StringType),
        th.Property("load_no", th.NumberType),
        th.Property("bol_no", th.StringType),
        th.Property("truck_no", th.StringType),
        th.Property("ticket_no", th.StringType),
        th.Property("stage_no", th.StringType),
        th.Property("gross_weight", th.NumberType),
        th.Property("carrier_name", th.StringType),
        th.Property("carrier_address", th.StringType),
        th.Property("driver_name", th.StringType),
        th.Property("added_by", th.StringType),
        th.Property("last_modified_by", th.StringType),
        th.Property("added_date", th.DateTimeType),
        th.Property("last_modified_date", th.DateTimeType),
        th.Property("load_status", th.StringType),
        th.Property("load_transfer_to_jobid", th.StringType),
        th.Property("load_transfer_from_jobid", th.StringType),
        th.Property("terminal_name", th.StringType),
        th.Property("approved_mileage", th.NumberType),
        th.Property("new_approved_mileage", th.NumberType),
        th.Property("approved_unload_time", th.NumberType),
        th.Property("product_name", th.StringType),
        th.Property("po_name", th.StringType),
        th.Property("product_unit", th.StringType),
        th.Property("weight", th.StringType),
        th.Property("job_name", th.StringType),
        th.Property("job_order_no", th.StringType),
        th.Property("destination_name", th.StringType),
        th.Property("destination_countryid", th.NumberType),
        th.Property("customer_name", th.StringType),
        th.Property("ordered_on", th.DateTimeType),
        th.Property("assigned_on", th.DateTimeType),
        th.Property("assigned_by", th.StringType),
        th.Property("accepted_on", th.DateTimeType),
        th.Property("accepted_by", th.StringType),
        th.Property("declined_on", th.DateTimeType),
        th.Property("declined_by", th.StringType),
        th.Property("terminal_on", th.DateTimeType),
        th.Property("terminal_comment", th.StringType),
        th.Property("terminal_by", th.StringType),
        th.Property("transit_on", th.DateTimeType),
        th.Property("transit_comment", th.StringType),
        th.Property("transit_by", th.StringType),
        th.Property("destination_on", th.DateTimeType),
        th.Property("destination_comment", th.StringType),
        th.Property("destination_by", th.StringType),
        th.Property("delivered_on", th.DateTimeType),
        th.Property("delivered_comment", th.StringType),
        th.Property("delivered_by", th.StringType),
        th.Property("transfer_on", th.DateTimeType),
        th.Property("transfer_comment", th.StringType),
        th.Property("transfer_by", th.StringType),
        th.Property("cancel_on", th.DateTimeType),
        th.Property("cancel_comment", th.StringType),
        th.Property("cancel_by", th.StringType),
        th.Property("load_boxes", th.ArrayType(
            th.ObjectType(
                th.Property("box_number", th.StringType),
                th.Property("box_weight", th.NumberType)
            )
        )),
        th.Property("total_boxes", th.NumberType),
        th.Property("silo_number", th.NumberType),
        th.Property("trailer_number", th.StringType),
        th.Property("appt_time", th.StringType),
        th.Property("at_terminal_offline", th.NumberType),
        th.Property("at_transit_offline", th.NumberType),
        th.Property("at_destination_offline", th.NumberType),
        th.Property("at_delivered_offline", th.NumberType),
        th.Property("rate", th.StringType),
        th.Property("rate_override_reason", th.StringType),
        th.Property("customer_rate", th.NumberType),
        th.Property("demurrage_at_loader_hour", th.NumberType),
        th.Property("demurrage_at_loader_minutes", th.NumberType),
        th.Property("demurrage_at_loader", th.StringType),
        th.Property("loader_override_reason", th.StringType),
        th.Property("demurrage_at_destination_hour", th.NumberType),
        th.Property("demurrage_at_destination_minutes", th.NumberType),
        th.Property("demurrage_at_destination", th.StringType),
        th.Property("destination_override_reason", th.StringType),
        th.Property("deadhead_cost", th.StringType),
        th.Property("override_cost", th.StringType),
        th.Property("override_reason", th.StringType),
        th.Property("dispatcher_notes", th.StringType),
        th.Property("total_hauling", th.StringType),
        th.Property("fuel_surcharge_cost", th.StringType),
        th.Property("total_charge", th.StringType),
        th.Property("driver_ticket_auto_status_name", th.StringType),
        th.Property("driver_ticket_auto_status_by", th.StringType),
        th.Property("terminal_total_time", th.StringType),
        th.Property("terminal_off_time", th.StringType),
        th.Property("transit_total_time", th.StringType),
        th.Property("transit_off_time", th.StringType),
        th.Property("destination_total_time", th.StringType),
        th.Property("destination_off_time", th.StringType),
        th.Property("ticket_manual_status_name", th.StringType),
        th.Property("ticket_manual_status_time", th.StringType),
        th.Property("driver_ticket_auto_status_time", th.StringType),
        th.Property("load_last_updated_date", th.DateTimeType),
        th.Property("region_name", th.StringType),
        th.Property("carrier_id", th.StringType),
        th.Property("driver_id", th.StringType),
        th.Property("terminal_id", th.StringType),
        th.Property("product_id", th.StringType),
        th.Property("destination_id", th.StringType),
        th.Property("customer_id", th.StringType),
        th.Property("load_status_id", th.NumberType),
        th.Property("ticket_is_missing", th.BooleanType),
        th.Property("job_id", th.StringType),
    ).to_dict()

    def post_process(self, row, context):
        row["job_id"] = context["job_id"]
        return row
class JobBoxesStream(PropXStream):
    name = "job_boxes"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    parent_stream_type = JobsStream
    path = "jobs/{job_id}/boxes"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("job_name", th.StringType),
        th.Property("destination_name", th.StringType),
        th.Property("address", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("status", th.NumberType),
        th.Property("loaded_box", th.StringType),
        th.Property("empty_box", th.StringType),
        th.Property("incoming_box", th.StringType),
        th.Property("product_wise",th.ArrayType(
            th.ObjectType(
                th.Property("product_id", th.StringType),
                th.Property("product_name", th.StringType),
                th.Property("on_location_count", th.NumberType),
                th.Property("on_location_weight", th.NumberType),
                th.Property("incoming_count", th.NumberType),
                th.Property("incoming_weight", th.NumberType),
                th.Property("delivered_count", th.NumberType),
                th.Property("delivered_weight", th.NumberType)
            )
        ))
    ).to_dict()

    def get_records(self, context: dict | None) -> t.Iterable[dict]:
        if context["working_status"] != "Active":
            return []
        else:
            return super().get_records(context)

    def post_process(self, row, context):
        row["job_id"] = context["job_id"]
        return row
    
class JobStagesStream(PropXStream):
    name = "job_stages"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    parent_stream_type = JobsStream
    path = "jobs/{job_id}/stages"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("form_no", th.StringType),
        th.Property("title", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("total_boxes", th.StringType),
        th.Property("final_boxes", th.StringType),
        th.Property("planned_boxes", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("company_name", th.StringType),
        th.Property("destination_id", th.StringType),
        th.Property("destination_name", th.StringType),
        th.Property("crew_id", th.NumberType),
        th.Property("start_time", th.DateTimeType),
        th.Property("end_time", th.DateTimeType),
        th.Property("stage_no", th.StringType),
        th.Property("status", th.NumberType),
        th.Property("report_status_id", th.NumberType),
        th.Property("report_status_name", th.StringType),
        th.Property("total_stages", th.StringType),
        th.Property("well_no", th.StringType),
        th.Property("last_modified_by", th.NumberType),
        th.Property("last_modified_date", th.StringType),
        th.Property("added_by", th.NumberType),
        th.Property("added_date", th.DateTimeType),
    ).to_dict()

    def get_records(self, context: dict | None) -> t.Iterable[dict]:
        if context["working_status"] != "Active":
            return []
        else:
            return super().get_records(context)
    
    def get_child_context(self, record: dict, context: dict | None) -> dict | None:
        return {
            "job_id":record['job_id'],
            "stage_id":record["id"]
        }    
class JobSilosStream(PropXStream):
    name = "job_silos"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    parent_stream_type = JobsStream
    path = "silo"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("serial_number", th.StringType),
        th.Property("current_job", th.StringType),
        th.Property("current_weight", th.StringType),
        th.Property("current_product", th.StringType),
        th.Property("current_belt_assignment", th.StringType),
    ).to_dict()

    def get_records(self, context: dict | None) -> t.Iterable[dict]:
        if context["working_status"] != "Active":
            return []
        else:
            return super().get_records(context)

    def post_process(self, row, context):
        row["job_id"] = context["job_id"]
        return row    

class JobStagesReportStream(PropXStream):
    name = "job_stage_report"
    records_jsonpath = "$.data[*]"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    parent_stream_type = JobStagesStream
    path = "jobs/{job_id}/stages/{stage_id}"

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("form_no", th.StringType),
        th.Property("title", th.StringType),
        th.Property("job_id", th.StringType),
        th.Property("total_boxes", th.StringType),
        th.Property("final_boxes", th.StringType),
        th.Property("planned_boxes", th.StringType),
        th.Property("company_id", th.StringType),
        th.Property("company_name", th.StringType),
        th.Property("destination_id", th.StringType),
        th.Property("destination_name", th.StringType),
        th.Property("crew_id", th.NumberType),
        th.Property("start_time", th.DateTimeType),
        th.Property("end_time", th.DateTimeType),
        th.Property("stage_no", th.StringType),
        th.Property("status", th.NumberType),
        th.Property("report_status_id", th.NumberType),
        th.Property("report_status_name", th.StringType),
        th.Property("total_stages", th.StringType),
        th.Property("well_no", th.StringType),
        th.Property("last_modified_by", th.NumberType),
        th.Property("last_modified_date", th.StringType),
        th.Property("added_by", th.NumberType),
        th.Property("added_date", th.DateTimeType),
        th.Property("product_summary", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()
    
    def get_next_page_token(self, response, previous_token=0):
        return None
    
class PumpingStatusLogStream(PropXStream):
    name = "pumping_status_log"
    path = "pumping_status_log"
    records_jsonpath = "$.data[*]"

    parent_stream_type = JobsStream
    ignore_parent_replication_keys = False
    loads_dict = {}
    path = "jobs/{job_id}/pumping_status_log"

    schema = th.PropertiesList(
        th.Property("pumping_status_log", th.ObjectType(
            th.Property("pumping_status_events", th.ArrayType(
                th.ObjectType(
                    th.Property("status", th.StringType),
                    th.Property("description", th.StringType),
                    th.Property("category", th.StringType),
                    th.Property("event_time", th.StringType),
                    th.Property("down_time", th.NumberType),
                    th.Property("last_modified_date", th.StringType),
                    th.Property("added_by", th.StringType),
                    th.Property("last_modified_by", th.StringType),
                )
            )),
            th.Property("total_down_time", th.NumberType),
        )),
        th.Property("job_id", th.StringType),
    ).to_dict()
    
    def post_process(self, row, context):
        row["job_id"] = context["job_id"]
        return row