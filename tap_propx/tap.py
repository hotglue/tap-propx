"""PropX tap class."""

from __future__ import annotations

from singer_sdk import Tap
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_propx import streams


class TapPropX(Tap):
    """PropX tap class."""

    name = "tap-propx"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The API Key to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        )
    ).to_dict()

    def discover_streams(self) -> list[streams.PropXStream]:
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.JobsStream(self),
            streams.LoadsStream(self),
            streams.JobBoxesStream(self),
            streams.JobStagesStream(self),
            streams.JobSilosStream(self),
            streams.JobStagesReportStream(self),
            streams.PumpingStatusLogStream(self),
        ]


if __name__ == "__main__":
    TapPropX.cli()
